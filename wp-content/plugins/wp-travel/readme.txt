=== WP Travel ===
Contributors: WEN Solutions
Tags: travel, Travel booking, Tour Operator, travel agency, Tours, tour,  itinerary, tour itinerary, tour operator, tour itineraries, accommodation, accommodation listings, destinations, regions, tourism, locations, map, trip book, tabs
Donate link: http://wptravel.io/downloads/
Requires at least: 4.4.0
Tested up to: 4.9.4
Stable tag: 1.2.2
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

The best choice for a Travel Agency, Tour Operator or Destination Management Company, wanting to manage packages more efficiently & increase sales.

== Description ==
"WP Travel" is the best choice for a Travel Agency, Tour Operator or Destination Management Company, wanting to manage packages more efficiently & increase sales. It's scalable, powerful, easy to use plugin for WordPress to manage packages and booking. Make a decision to enhance your travel site with powerful features with our simple plugin.

* Check detail info at ["wptravel.io"](http://wptravel.io/) by WEN Solution.
* Checkout ["Demo"](http://wptravel.io/demo).

= "WP TRAVEL" COMPATIBLE THEMES: =
* ["Travel Log"](http://wensolutions.com/themes/travel-log/) by WEN Solutions.

= Some important features of the plugin: =
* Tour packages with no limit: Create unlimited number of tour packages (that your server can store). Every detail you need for a trip like description, itinerary builder, price, sales price, duration, gallery, package included and excluded, map and many more can be entered very easily.
* Tour filters: User can find their required Itineraries by filtering or using easy search form.
* Painless Booking System: WP Travel include very easy and simple booking system which user can fill up very fast and get you more bookings.
* Full Data Reporting: Data are very important for all business. WP Travel has in-build booking stat that helps you to generate the report from different date range, types and locations.
* Payment System: Currently we have Standard PayPal as a free addon (which later will be included in WP Travel). It can be download from ["here"](http://wptravel.io/downloads/standard-paypal/). If you want to more payment system please ["let us know"](http://wptravel.io/support-forum/).
* Email Notification: You as an admin will be notified of all booking made by users and also users will also get email notification about their booking details. All email templates are customizable.
* Customizable tabs: All trip details page tabs in front end can be customized from backend. You can change their labels and sort by your requirements or hide them.
* Rating & Reviews: You can allow users to add rating or reviews for each trips. You can also hide them if you want for each trips.
* Map: For each trips you can show Google map. In front end user can see where is the location of trip.

= Other features: =
* Featured trips
* FAQs
* Image Gallery
* Group size
* Set Currency
* Pricing per person & group
* Sale price
* Set fix departures
* Trip Types
* Trip Locations
* Keywords
* Itineraries filters
* Translation Ready
* Complete documentation
* Customizable
* Fluid Layout, Responsive
* Compatible Themes
* And more ...


= Customization and Flexibility =
WP Travel plugin is very flexible and highly customizable. There are a number of hooks and filters which makes plugin really flexible from the developer's point of view. There are bulk of options which add further more flexibility in the plugin.

= Supported Themes =
["Travel Log"](http://wensolutions.com/themes/travel-log/) theme is specially designed for “WP Travel” plugin. So, it is 100% compatible with our plugin. It can be used with other themes as well with some styling changes. We are always here to support with any issues using the theme.


= Dedicated Support and Documentation =
We have an entire team of happiness engineers ready to help you. Ask your questions in the [support forum](http://wptravel.io/support-forum/forum/wp-travel/).

Also you can checkout WP Travel [documentation](http://wptravel.io/documentations/).

= Release Notes: =
Before updating plugin please check ["our latest release notes"](http://wptravel.io/category/wp-travel-release/).

== Installation ==
= Using The WordPress Dashboard =
* Navigate to the 'Add New' in the plugins dashboard
* Search for "WP Travel"
* Click Install Now
* Activate the plugin on the Plugin dashboard

= Uploading in WordPress Dashboard =
* Navigate to the 'Add New' in the plugins dashboard
* Navigate to the 'Upload' area
* Select wp-travel.zip from your computer
* Click 'Install Now'
* Activate the plugin in the Plugin dashboard

= Using FTP =
* Download wp-travel.zip
* Extract the wp-travel directory to your computer
* Upload the wp-travel directory to the /wp-content/plugins/ directory
* Activate the plugin in the Plugin dashboard

== Frequently Asked Questions ==
= There is something cool you could add... =

Nice, send us request on [Support page](http://wptravel.io/support-forum/forum/wp-travel/ "WEN Solutions Support") and let us know. We are always looking for ways to improve our plugins.

== Screenshots ==
1. Backend: New itinerary
2. Backend: New itinerary, Additional info
3. Backend: New itinerary, Images/ gallery
4. Backend: New itinerary, Location
5. Backend: Itinerary listing
6. Backend: Trip types
7. Backend: Locations
8. Backend: Booking listing
9. Backend: Booking details
10. Frontend: Itinerary listing
11. Frontend: Itinerary details - 1
12. Frontend: Itinerary details - 2

== Upgrade Notice ==
Before updating plugin please check ["our latest release notes"](http://wptravel.io/category/wp-travel-release/).

== Changelog ==

= 1.2.2 =
* New Feature: Email Template options for booking,payments and enquiries.
* New Feature: TAX Options for Trip Prices.
* Fixes: Payment issue fix while null or empty trip price.
* Fixes: Date format issue fixes for fixed departure date.
* Fixes: Reviews meta disabled when reviews tab is disabled in trip details.
* Fixes: Minor issues and bugs fixings.

= 1.2.1 =
* New Feature: WP Travel Standard PayPal addon merged to core.
* New Feature: Archive page widget area added.
* New Feature: New WP Travel Filters Widget added for trips search.
* New Feature: New WP Travel Filters Widget shortcode added.
* New Feature: New menu Docs & Support added.
* Fixes: Minor issues and bugs fixings.

= 1.2.0 =
* New Feature: Sale widget added.
* New Feature: Inquiry Form added.
* New Feature: Date and time added in itinerary timeline.
* New Feature: Compare Price on Stat.
* Fixes: Tabs Sortable issue.
* Fixes: Embeded frame allowed in frontend overview.
* Fixes: Minor issues.

= 1.1.3 =
* New Feature: Itinerary Timeline added.
* Fixes: Issue with WP Travel Standard Paypal.

= 1.1.2 =
* New Feature: Frontend Tab label update and sorting.
* New Feature: FAQ.
* Fixes: Comment rating issue.

= 1.1.1 =
* Fixes: PHP Error below PHP Ver 5.4.

= 1.1 =
* New Feature: "Loco Translate" support.
* New Feature: "WPML Translate" support.
* New Feature: Payment fields added globally.
* New Feature: Activities Taxonomy added.
* New Feature: Slug override for trip, location, trip type, activities.
* Changes: Itineraries post type changed to trip.
* Changes: Price fields moved to price tab in itineraries section.
* Fixes: Thumbnail blur issue.
* Fixes: Booking Count on post delete.

= 1.0.6 =
* New Feature: Booking Status added.
* Fixes: Default view mode [grid] style issue.
* Fixes: Deprecated Functions issue.
* Fixes: Booking Stat issue with query.
* Fixes: Minor layout issue.

= 1.0.5 =
* New Feature: Booking Stat.
* New Feature: Featured itinerary widget.
* New Feature: Trip Location widget.
* New Feature: Trip Type widget.
* New Feature: Compatibility with WP Travel PayPal add-ons.
* New Feature: Added Form field for radio and checkbox input.
* New Feature: Added PAX field in Booking form.
* Fixes: PHP error on lower PHP version.
* Fixes: Layout issue [itinerary list per row on grid view ].
* Fixes: Backend Map issue.
* Fixes: Minor fixes.

= 1.0.4 =
* New Feature: Filter Itineraries by Price, Trip Type, Location.
* New Feature: Archive Page View Mode. Default 'list' [grid/list].
* New Feature: Booking form fields filter.
* New Feature: Location displayed in single page.
* New Feature: Currency option for 'KES'.
* New Feature: Added system information.
* Fixes: Text translation updated with new strings.
* Fixes: Trip Duration/Fixed Departure issue on frontend.
* Fixes: Template override issue with post not found.
* Fixes: Responsive issue on WP_TRAVEL_ITINERARIES.
* Fixes: Shortcode issue on WP_TRAVEL_ITINERARIES.
* Fixes: Related Itineraries issue with map.
* Fixes: Hash link updated for single itinerary tabs.
* Test: Tested upto 4.9.


= 1.0.3 =
* Fixes: Placed language file.
* Fixes: Label text updated for frontend itinerary tabs.
* Fixes: Fix translation issues.

= 1.0.2 =
* New Feature: Hide related itinerary option added in settings.
* New Feature: Option to send booking mail to admin.
* New Feature: Booking notification email sent to customer.
* New Feature: Keyword taxonomy.
* New Feature: Search Widgets.
* New Feature: Template override.
* New Feature: Currency option for 'TZS'.
* New Feature: [WP_TRAVEL_ITINERARIES] shortcode added.
* New Feature: Fixed Departure, Trip Duration options in Additional info tab.
* New Feature: Itinerary listing design updated.
* Fixes: Settings option not working in WooCommerce.
* Fixes: Minor design issues.

= 1.0.1 =
* New: Added feature on booking.
* New: Added featured itineraries. Featured itineraries can be fetched from "wp_travel_featured_itineraries" function.
* New: Added shortcode "WP_TRAVEL_ITINERARIES" to list itineraries by type, location or featured.
* Fixed: Minor issues and bugs.

= 1.0.0 =
* Initial  release.
