# WARNING! DO NOT PUT CUSTOM TRANSLATIONS HERE!

WP Travel will delete all custom translations placed in this directory.

## Translating WP Travel
Put your custom WP Travel translations in your WordPress language directory, located at: WP_LANG_DIR . "/wp-travel/{$textdomain}-{$locale}.mo";

## Contributing your translating to WP Travel
If you want to help translate WP Travel, please visit our [translation page](https://translate.wordpress.org/projects/wp-plugins/wp-travel).
