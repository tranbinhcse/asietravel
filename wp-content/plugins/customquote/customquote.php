<?php
/*
  Plugin Name: Custom quote
  Plugin URI: https://facebook.com/hanhdo205
  Description: Request a custom quote.
  Author: Nguyen Quoc Hanh
  Version: 1.0
  Author URI: https://facebook.com/hanhdo205
*/

require_once (__DIR__ . '/page4_insertdata.php');
require_once (__DIR__ . '/page3_form.php');
require_once (__DIR__ . '/page2_form.php');
require_once (__DIR__ . '/page1_form.php');  
  
  
function register_session(){
    if( !session_id() )
        session_start();
}
add_action('init','register_session');

function customquote_load_plugin_textdomain() {
    load_plugin_textdomain('customquote', FALSE, plugin_basename(dirname(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'customquote_load_plugin_textdomain');

function customquote_reg_scripts() {

  wp_register_style( 'customquote-css', plugin_dir_url( __FILE__) . 'css/customquote.css' );
  wp_enqueue_style( 'customquote-css' );

  wp_enqueue_script( 'customquote-script', plugin_dir_url( __FILE__) . 'js/customquote.js', array ( 'jquery' ), '09032018', true);
  wp_localize_script( 'customquote-script', 'ajax_object',
        array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('wp_enqueue_scripts', 'customquote_reg_scripts');

// Admin sub-menu
add_action('admin_init', 'customquote_admin_init');
add_action('admin_menu','customquote_add_page');

// White list our options using the Settings API
function customquote_admin_init() {
    register_setting('customquote_options', 'customquote');
}

// Add entry in the settings menu
function customquote_add_page() {
    add_options_page('customquote  Options', 'customquote Options', 'manage_options', 'customquote_options', 'customquote_options_do_page');
}

// Print the menu page itself
function customquote_options_do_page() {
    $options = get_option('customquote');
    //print_r($options);
    ?>
    <div class="wrap">
        <h2>Admin email</h2>
        <form method="post" action="options.php">
            <?php settings_fields('customquote_options'); ?>
            <table class="form-table">
                <tr valign="top">
                  <th scope="row">Email to receive customquotes:</th>
                    <td><input type="email" name="customquote[customquote_email]" value="<?php echo $options['customquote_email']; ?>" /><br>
                      <em>Leave blank for use dashboard admin</em></td>
                </tr>
            </table>
            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
            </p>
        </form>
    </div>
    <?php
}

// Create some of pages when plugin activated
function xt_plugin_install() {

    global $wpdb;

    $step1 = array('title' => 'Your Details', 'content' => '[your_detail_form]', 'slug' => 'your-details');
    $step2 = array('title' => 'Your Project', 'content' => '[your_project_form]', 'slug' => 'your-project');
    $step3 = array('title' => 'Contact', 'content' => '[your_contact_form]', 'slug' => 'your-contact');
    $step4 = array('title' => 'Thankyou', 'content' => '[thankyou_form]', 'slug' => 'thankyou');

    $pages = array($step1, $step2, $step3, $step4);

    foreach ($pages as $key => $value) {

        $the_page_title = $value['title'];
        $the_page_name = $value['slug'];

        // the menu entry...
        delete_option("xt_plugin_page_title");
        add_option("xt_plugin_page_title", $the_page_title, '', 'yes');
        // the slug...
        delete_option("xt_plugin_page_name");
        add_option("xt_plugin_page_name", $the_page_name, '', 'yes');
        // the id...
        delete_option("xt_plugin_page_id");
        add_option("xt_plugin_page_id", '0', '', 'yes');

        $the_page = get_page_by_title($the_page_title);

        if (!$the_page) {

            // Create post object
            $_p = array();
            $_p['post_title'] = $the_page_title;
            $_p['post_content'] = $value['content'];
            $_p['post_status'] = 'publish';
            $_p['post_type'] = 'page';
            $_p['comment_status'] = 'closed';
            $_p['ping_status'] = 'closed';
            $_p['post_category'] = array(1); // the default 'Uncatrgorised'
            // Insert the post into the database
            $the_page_id = wp_insert_post($_p);
        } else {
            // the plugin may have been previously active and the page may just be trashed...

            $the_page_id = $the_page->ID;

            //make sure the page is not trashed...
            $the_page->post_status = 'publish';
            $the_page_id = wp_update_post($the_page);
        }

        delete_option('xt_plugin_page_id');
        add_option('xt_plugin_page_id', $the_page_id);
    }
}

// Remove created of pages when plugin deactivated
function xt_plugin_remove() {

    global $wpdb;

    $the_page_title = get_option("xt_plugin_page_title");
    $the_page_name = get_option("xt_plugin_page_name");

    //  the id of our page...
    $the_page_id = get_option('xt_plugin_page_id');
    if ($the_page_id) {

        wp_delete_post($the_page_id); // this will trash, not delete
    }

    delete_option("xt_plugin_page_title");
    delete_option("xt_plugin_page_name");
    delete_option("xt_plugin_page_id");
}

/* Runs when plugin is activated */
register_activation_hook(__FILE__, 'xt_plugin_install');

/* Runs on plugin deactivation */
register_deactivation_hook(__FILE__, 'xt_plugin_remove');