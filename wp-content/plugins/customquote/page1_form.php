<?php
function add_your_detail_form() { 
  ob_start();
  ?>
 <div class="container">
 <div class="main">
 <h2><?php _e('Your Details','booking');?></h2>
 <span id="error">
 <!-- Initializing Session for errors -->
 <?php
 if (!empty($_SESSION['error'])) {
 echo $_SESSION['error'];
 unset($_SESSION['error']);
 }
 ?>
 </span>
 <form action="../your-project/" method="post">
 <div>	
 <label><?php _e('Your name and surname:','booking');?> <span>*</span></label>
 <select name="prefix">
 	<option>Mr</option>
 	<option>Mrs</option>
 </select>
 <input name="yourname" type="text" placeholder="Jimmy" required>
 <input name="surname" type="text" placeholder="Ngo" required>
</div>
<div>
 <label><?php _e('Your email address: ','booking');?> <span>*</span></label>
 <input name="email" type="email" placeholder="Ex-anderson@gmail.com" required>
 </div>
<div>
 <label><?php _e('Your country:','booking');?> <span>*</span></label>
 <select name="country">
 	<option>Vietnam</option>
 </select>
 <label><?php _e('Department:','booking');?> <span>*</span></label>
 <select name="department">
 	<option>To select</option>
 </select>
 <label><?php _e('Your city:','booking');?> <span>*</span></label>
 <input name="city" type="text" required>
 </div>
 <input type="reset" value="Reset" />
 <input type="submit" value="Next" />
 </form>
 </div>
 </div>

<?php 
$result = ob_get_contents();
ob_end_clean();
return $result;

} 

add_shortcode('your_detail_form','add_your_detail_form');