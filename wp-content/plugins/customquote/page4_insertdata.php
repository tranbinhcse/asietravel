<?php
function add_thankyou_form() { 
  ob_start();
  ?>
 <div class="container">
 <div class="main">
 <?php
 
 if (!empty($_SESSION['post'])){
 
 foreach ($_POST as $key => $value) {
 $_SESSION['post'][$key] = $value;
 } 
 extract($_SESSION['post']); // Function to extract array.
 if ($yourname) {
 	$options = get_option('customquote');

 	$admin_email = (!empty($options['customquote_email'])) ? $options['customquote_email'] : get_option( 'admin_email' );
 	$emailtitle = 'Request a custom quote';
 	$headers = array(
    'From: ' . $admin_email, 
    'CC: '.$email, 
    'Content-type: text/html', 
    );
    $emailcontent = 'From: ' . $prefix . '.' . $yourname . ' ' . $surname . '<br />';
    $emailcontent .= 'Email: ' . $email . '<br />';
    $emailcontent .= 'Country: ' . $country . '<br />';
    $emailcontent .= 'Department: ' . $department . '<br />';
    $emailcontent .= 'City: ' . $city . '<br />';
    $emailcontent .= 'Arrival approximate: ' . $arrival . '<br />';
    $emailcontent .= 'Return date: ' . $return . '<br />';
    $emailcontent .= 'Duration of the trip: ' . $duration . '<br />';
    $emailcontent .= 'Destination(s): ' . $dest . '<br /><br />';
    $emailcontent .= 'Describe your project, your vision of the trip and how you want to discover our country: ' . $describe . '<br /><br />';
    $emailcontent .= 'Have you already bought your international return ticket(s)? ' . $returnticket . '<br /><br />';
    $emailcontent .= 'The particapants:<br />';
    $emailcontent .= 'Adult(s) (>12 years old): ' . $adult . '<br />';
    $emailcontent .= 'Child(s) (2 - 12 years old): ' . $child . '<br />';
    $emailcontent .= 'Baby(s) (<2 years old): ' . $baby . '<br />';
    $emailcontent .= 'Details of ages: ' . $detailofage . '<br /><br />';
    $emailcontent .= 'What kind of accommodation would you like for this trip? ' . $accommodation . '<br /><br />';
    $emailcontent .= 'How many rooms do you want:<br />';
    $emailcontent .= 'Double room with a large bed: ' . $large . '<br />';
    $emailcontent .= 'Double room with 2 beds: ' . $twobed . '<br />';
    $emailcontent .= 'Room for 3 people: ' . $threepeople . '<br />';
    $emailcontent .= 'House detached: ' . $detached . '<br /><br />';
    $emailcontent .= 'Meal:<br />';
    $emailcontent .= 'Breakfast is ussually already includedin the price accommodation. Would you like other meals? ' . $meal . '<br /><br />';
    $emailcontent .= 'Budget per person:<br />';
    $emailcontent .= 'Put a ceiling amount if you want an offer based on your budget (Total budget, including round-trip international flights): ' . $budget . '<br /><br />';
    $emailcontent .= 'TO KNOW YOU BETTER<br /><br />';
    $emailcontent .= 'Can you tell us about your last long-haul trip? (destination, type of trip, experiences, what you liked,...): ' . $longhaul . '<br /><br />';
    $emailcontent .= 'Your favorite hobbies and hobbies (what you like, what you do not like...): ' . $hobbies . '<br /><br />';
    $emailcontent .= 'AGREE A TELEPHONE APPOINTMENT<br /><br />';
    $emailcontent .= 'We call you back for free to better understand your project<br />';
    $emailcontent .= 'Can we agree on a phone interview? ' . $agree . '<br /><br />';
    $emailcontent .= 'If you are recommended by a former client of Amica, please specify his name and surname: ' . $recommended . '<br /><br />';
    $emailcontent .= 'Newsleter:<br />';
    $emailcontent .= 'I would not like to receive information (reports, promotions, travel advice...) from Aminca travel. (1 time per week): ' . $newsleter . '<br />';
 	wp_mail( $admin_email, $emailtitle, $emailcontent, $headers );
 	echo '<p><span id="success">Form Submitted successfully..!!</span></p>';
 } else {
 echo '<p><span>Form Submission Failed..!!</span></p>';
 } 
 unset($_SESSION['post']); // Destroying session.
 
 } else {
 	wp_redirect( site_url() . '/your-details/');
	exit; // Redirecting to first page.
 }
 
 ?>
 </div>
 </div>
 
<?php 
$result = ob_get_contents();
ob_end_clean();
return $result;

} 

add_shortcode('thankyou_form','add_thankyou_form');