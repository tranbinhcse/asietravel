<?php
function add_your_project_form() { 
  ob_start();
// Checking first page values for empty,If it finds any blank field then redirected to first page.
if (isset($_POST['yourname'])){
 if (empty($_POST['yourname'])
 || empty($_POST['surname'])
 || empty($_POST['email'])
 || empty($_POST['city'])){ 
 // Setting error message
 $_SESSION['error'] = "Mandatory field(s) are missing, Please fill it again";
 	wp_redirect( site_url() . '/your-details/');
	exit; // Redirecting to first page 
 } else {
 // Sanitizing email field to remove unwanted characters.
 $_POST['email'] = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL); 
 // After sanitization Validation is performed.
 if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){ 
 	foreach ($_POST as $key => $value) {
	 $_SESSION['post'][$key] = $value;
	 }
 } else {
 $_SESSION['error'] = "Invalid Email Address";
 	wp_redirect( site_url() . '/your-details/');
	exit;//redirecting to first page
 }
 }
} else {
 if (empty($_SESSION['error_page2'])) {
 	wp_redirect( site_url() . '/your-details/');
	exit;//redirecting to first page
 }
}?>

 <div class="container">
 <div class="main">
 	<a href="javascript:void(0)" onclick="goBack()"><?php _e('Back to the previous page','booking');?></a>
 <h2>Your Select</h2><hr/>
 <span id="error">
<?php
// To show error of page 2.
if (!empty($_SESSION['error_page2'])) {
 echo $_SESSION['error_page2'];
 unset($_SESSION['error_page2']);
}
?>
 </span>
 <form action="../your-contact/" method="post">
 <div>	
 <label><?php _e('Arrival approximate :','booking');?><span>*</span></label>
 <input name="arrival" id="arrival" type="date" value="" >
 <label><?php _e('Return date :','booking');?><span>*</span></label>
 <input name="return" id="return" type="date" value="" >
 <label><?php _e('Duration of the trip :','booking');?><span>*</span></label>
 <select name="duration">
 	<option>0</option>
 	<option>1</option>
 </select>
</div>
<div>
	<label><?php _e('Destination(s) :','booking');?><span>*</span></label>
	 	<input type="radio" name="dest" value="Vietnam" checked> Vietnam<br>
  		<input type="radio" name="dest" value="Lao"> Lao<br>
  		<input type="radio" name="dest" value="Multi-country"> <?php _e('Multi-country: indicate the desired destinations','booking');?>
  		<select name="multicountry">
		 	<option>To select</option>
		 </select>
</div>
<div>
	<label><?php _e('Describe your project, your vision of the trip and how you want to discover our country :','booking');?><span>*</span></label>
	<textarea rows="5" cols="50" name="describe"></textarea>
</div>
<div>
	<label><?php _e('Have you already bought your international return ticket(s)?','booking');?></label>
		<input type="radio" name="returnticket" value="yes" checked> Yes<br>
  		<input type="radio" name="returnticket" value="no"> No
</div>
<div>
	<label><?php _e('The particapants :','booking');?><span>*</span></label>
		<?php _e('Adult(s) (>12 years old)','booking');?>
		<select name="adult">
		 	<option>0</option>
		 	<option>1</option>
		 </select>
		 <?php _e('Child(s) (2 - 12 years old)','booking');?>
		 <select name="child">
		 	<option>0</option>
		 	<option>1</option>
		 </select>
		 <?php _e('Baby(s) (<2 years old)','booking');?>
		 <select name="baby">
		 	<option>0</option>
		 	<option>1</option>
		 </select>
</div>
<div>
	<label><?php _e('Details of ages:','booking');?></label>
 	<input name="detailofage" id="detailofage" type="text" value="" >
</div>
<div>
	<label><?php _e('What kind of accommodation would you like for this trip?:','booking');?></label>
 	<input type="checkbox" name="accommodation" value="2star">2 star > 30 and < 40 EUR<br>
	<input type="checkbox" name="accommodation" value="3star">3 star > 40 and < 60 EUR
</div>
<div>
	<label><?php _e('How many rooms do you want :','booking');?><span>*</span></label>
		<?php _e('Double room with a large bed','booking');?>
		<select name="large">
		 	<option>0</option>
		 	<option>1</option>
		 </select>
		 <?php _e('Double room with 2 beds','booking');?>
		 <select name="twobed">
		 	<option>0</option>
		 	<option>1</option>
		 </select>
		 <?php _e('Room for 3 people','booking');?>
		 <select name="threepeople">
		 	<option>0</option>
		 	<option>1</option>
		 </select>
		 <?php _e('House detached','booking');?>
		 <select name="detached">
		 	<option>0</option>
		 	<option>1</option>
		 </select>
</div>
<div>
	<label><?php _e('Meal :','booking');?><span>*</span></label>
	<?php _e('Breakfast is ussually already includedin the price accommodation. Would you like other meals?','booking');?>
	<select name="meal">
		 	<option>No</option>
		 	<option>Yes</option>
		 </select>
</div>
<div>
	<label><?php _e('Budget per person :','booking');?><span>*</span></label>
	<?php _e('Put a ceiling amount if you want an offer based on your budget (Total budget, including round-trip international flights)','booking');?>
	<input name="budget" id="budget" type="text" value="" >
	euros/person
</div>
 <input type="button" onclick="goBack()" value="Back" />
 <input type="reset" value="Reset" />
 <input type="submit" value="Next step" />
 </form>
 </div>
 </div>
	
<?php 
$result = ob_get_contents();
ob_end_clean();
return $result;

} 

add_shortcode('your_project_form','add_your_project_form');