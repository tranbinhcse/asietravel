<?php
function add_your_contact_form() { 
  ob_start();
// Checking second page values for empty, If it finds any blank field then redirected to second page.
if (isset($_POST['duration'])){
 if (empty($_POST['duration'])
 || empty($_POST['describe'])){ 
 $_SESSION['error_page2'] = "Mandatory field(s) are missing, Please fill it again"; // Setting error message.
 	wp_redirect( site_url() . '/your-project/');
	exit; // Redirecting to second page. 
 } else {
 // Fetching all values posted from second page and storing it in variable.
 foreach ($_POST as $key => $value) {
 $_SESSION['post'][$key] = $value;
 }
 }
} else {
 if (empty($_SESSION['error_page3'])) {
 	wp_redirect( site_url() . '/your-details/');
	exit; // Redirecting to first page.
 }
}
?>

 <div class="container">
 <div class="main">
 	<a href="javascript:void(0)" onclick="goBack()"><?php _e('Back to the previous page','booking');?></a>
 <h2>Contact</h2><hr/>
 <span id="error">
 <?php
 if (!empty($_SESSION['error_page3'])) {
 echo $_SESSION['error_page3'];
 unset($_SESSION['error_page3']);
 }
 ?>
 </span>
 <form action="../thankyou/" method="post">
 <b><?php _e('TO KNOW YOU BETTER','booking');?></b>
 <div>
	<label><?php _e('Can you tell us about your last long-haul trip? (destination, type of trip, experiences, what you liked,...)','booking');?></label>
	<textarea rows="5" cols="50" name="longhaul"></textarea>
</div>
<div>
	<label><?php _e('Your favorite hobbies and hobbies (what you like, what you do not like...)','booking');?></label>
	<textarea rows="5" cols="50" name="hobbies"></textarea>
</div>
<b><?php _e('AGREE A TELEPHONE APPOINTMENT','booking');?></b>
<div>
	<label><?php _e('We call you back for free to better understand your project','booking');?></label>
	<?php _e('Can we agree on a phone interview','booking');?>
		<input type="radio" name="agree" value="Yes" checked> Yes<br>
  		<input type="radio" name="agree" value="No"> No thanks
</div>
<div>
	<label><?php _e('If you are recommended by a former client of Amica, please specify his name and surname','booking');?></label>
	<textarea rows="5" cols="50" name="recommended"></textarea>
</div>
<div>
	<label>Newsleter</label>
 	<input type="checkbox" name="newsleter" value="Yes"><?php _e('I would not like to receive information (reports, promotions, travel advice...) from Aminca travel. (1 time per week)','booking');?> 
</div>
 
 <input type="reset" value="Reset" />
 <input name="submit" type="submit" value="Send request" />
 </form>
 </div> 
 </div>
 
<?php 
$result = ob_get_contents();
ob_end_clean();
return $result;

} 

add_shortcode('your_contact_form','add_your_contact_form');