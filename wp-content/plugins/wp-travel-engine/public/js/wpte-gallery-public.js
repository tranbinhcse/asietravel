jQuery(document).ready(function($){
$(".feat-img-gallery-holder").slick({
    autoplay: true,
    dots: true,
    customPaging : function(slider, i) {
        var thumb = $(slider.$slides[i]).data('thumb');
        return '<a><img src="'+thumb+'"></a>';
    },
    responsive: [{ 
        breakpoint: 500,
        settings: {
        	autoplay: false,
            dots: false,
            arrows: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1
        } 
    }]
});
});

