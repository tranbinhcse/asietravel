<div class="row large-columns-1 medium-columns-1 small-columns-1">

<div class="col post-item">
			<div class="col-inner">
			<a href="<?php the_permalink(); ?>" class="plain">
				<div class="box box-vertical box-text-bottom box-visiter-post has-hover">
            					<div class="box-image" style="width:35%;">
  						<div class="image-cover" style="padding-top:56%;">
                <?php the_post_thumbnail( 'medium', $attr = 'sizes=(max-width: 300px) 100vw, 300px' ) ?>
              </div>
              </div><!-- .box-image -->
          					<div class="box-text text-left">
					<div class="box-text-inner visiter-post-inner">

										<h5 class="post-title is-large "><?php the_title() ?></h5>
										<!-- <div class="is-divider"></div> -->
										<p class="from_the_visiter_excerpt "><?php echo get_the_excerpt() ?></p>



					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div>
  </div>
