<?php
// Add custom Theme Functions here
// Register Script
function wbc_scripts() {

	wp_register_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', false, '4.0.6', false );
  wp_enqueue_script( 'select2' );
	wp_register_script( 'scripts-custom', get_stylesheet_directory_uri().'/scripts.js', false, '4.0.6', false );
  wp_enqueue_script( 'scripts-custom' );
	wp_register_style( 'select2-css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', $deps = array(), $ver = '4.0.6', $media = 'all' );
    wp_enqueue_style( 'select2-css' );
}
add_action( 'wp_enqueue_scripts', 'wbc_scripts' );


if( ! class_exists( 'Showcase_Taxonomy_Images' ) ) {
  class Showcase_Taxonomy_Images {

    public function __construct() {
     //
    }

    /**
     * Initialize the class and start calling our hooks and filters
     */
     public function init() {
     // Image actions
     add_action( 'destination_add_form_fields', array( $this, 'add_category_image' ), 10, 2 );
     add_action( 'created_destination', array( $this, 'save_category_image' ), 10, 2 );
     add_action( 'destination_edit_form_fields', array( $this, 'update_category_image' ), 10, 2 );
     add_action( 'edited_destination', array( $this, 'updated_category_image' ), 10, 2 );
     add_action( 'admin_enqueue_scripts', array( $this, 'load_media' ) );
     add_action( 'admin_footer', array( $this, 'add_script' ) );
   }

   public function load_media() {
     if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'destination' ) {
       return;
     }
     wp_enqueue_media();
   }

   /**
    * Add a form field in the new category page
    * @since 1.0.0
    */

   public function add_category_image( $taxonomy ) { ?>
     <div class="form-field term-group">
       <label for="showcase-taxonomy-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
       <input type="hidden" id="showcase-taxonomy-image-id" name="showcase-taxonomy-image-id" class="custom_media_url" value="">
       <div id="category-image-wrapper"></div>
       <p>
         <input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Add Image', 'showcase' ); ?>" />
         <input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
       </p>
     </div>
   <?php }

   /**
    * Save the form field
    * @since 1.0.0
    */
   public function save_category_image( $term_id, $tt_id ) {
     if( isset( $_POST['showcase-taxonomy-image-id'] ) && '' !== $_POST['showcase-taxonomy-image-id'] ){
       add_term_meta( $term_id, 'showcase-taxonomy-image-id', absint( $_POST['showcase-taxonomy-image-id'] ), true );
     }
    }

    /**
     * Edit the form field
     * @since 1.0.0
     */
    public function update_category_image( $term, $taxonomy ) { ?>
      <tr class="form-field term-group-wrap">
        <th scope="row">
          <label for="showcase-taxonomy-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
        </th>
        <td>
          <?php $image_id = get_term_meta( $term->term_id, 'showcase-taxonomy-image-id', true ); ?>
          <input type="hidden" id="showcase-taxonomy-image-id" name="showcase-taxonomy-image-id" value="<?php echo esc_attr( $image_id ); ?>">
          <div id="category-image-wrapper">
            <?php if( $image_id ) { ?>
              <?php echo wp_get_attachment_image( $image_id, 'thumbnail' ); ?>
            <?php } ?>
          </div>
          <p>
            <input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Add Image', 'showcase' ); ?>" />
            <input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
          </p>
        </td>
      </tr>
   <?php }

   /**
    * Update the form field value
    * @since 1.0.0
    */
   public function updated_category_image( $term_id, $tt_id ) {
     if( isset( $_POST['showcase-taxonomy-image-id'] ) && '' !== $_POST['showcase-taxonomy-image-id'] ){
       update_term_meta( $term_id, 'showcase-taxonomy-image-id', absint( $_POST['showcase-taxonomy-image-id'] ) );
     } else {
       update_term_meta( $term_id, 'showcase-taxonomy-image-id', '' );
     }
   }

   /**
    * Enqueue styles and scripts
    * @since 1.0.0
    */
   public function add_script() {
     if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'destination' ) {
       return;
     } ?>
     <script> jQuery(document).ready( function($) {
       _wpMediaViewsL10n.insertIntoPost = '<?php _e( "Insert", "showcase" ); ?>';
       function ct_media_upload(button_class) {
         var _custom_media = true, _orig_send_attachment = wp.media.editor.send.attachment;
         $('body').on('click', button_class, function(e) {
           var button_id = '#'+$(this).attr('id');
           var send_attachment_bkp = wp.media.editor.send.attachment;
           var button = $(button_id);
           _custom_media = true;
           wp.media.editor.send.attachment = function(props, attachment){
             if( _custom_media ) {
               $('#showcase-taxonomy-image-id').val(attachment.id);
               $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
               $( '#category-image-wrapper .custom_media_image' ).attr( 'src',attachment.url ).css( 'display','block' );
             } else {
               return _orig_send_attachment.apply( button_id, [props, attachment] );
             }
           }
           wp.media.editor.open(button); return false;
         });
       }
       ct_media_upload('.showcase_tax_media_button.button');
       $('body').on('click','.showcase_tax_media_remove',function(){
         $('#showcase-taxonomy-image-id').val('');
         $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
       });
       // Thanks: http://stackoverflow.com/questions/15281995/wordpress-create-category-ajax-response
       $(document).ajaxComplete(function(event, xhr, settings) {
         var queryStringArr = settings.data.split('&');
         if( $.inArray('action=add-tag', queryStringArr) !== -1 ){
           var xml = xhr.responseXML;
           $response = $(xml).find('term_id').text();
           if($response!=""){
             // Clear the thumb image
             $('#category-image-wrapper').html('');
           }
          }
        });
      });
    </script>
   <?php }
  }
$Showcase_Taxonomy_Images = new Showcase_Taxonomy_Images();
$Showcase_Taxonomy_Images->init();
}

//add_action( 'destination_add_form_fields', array( $this, 'add_category_image' ), 10, 2 );

class WBC_Parent_Page_Walker extends Walker_Page {

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);

        if ($depth == 0) {
            $output .= "\n$indent\n";
        } else {
            $output .= "\n$indent\n";
        }
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);

        if ($depth == 0) {
            $output .= "$indent\n";
        } else {
            $output .= "$indent\n";
        }
    }
}

function wbc_list_child_pages() {
  global $post;
  if ( is_page() && $post->post_parent){
    $childpages = '';

    if (count(get_post_ancestors($post->ID))>1) {
      $p = get_page($post->post_parent);
      $t = $p->post_title;
      $l = get_page_link($post->post_parent);
      $childpages .= '<li class="page_item"><a href="'.$l.'">'.apply_filters('post_title', $t).'</a></li>';
    }


    $childpages .= wp_list_pages(array(
      'title_li' => null,
      'sort_column' => 'menu_order',
      'echo' => 0,
      //'depth' => 2,
      'child_of' => $post->post_parent,
      'walker' => new WBC_Parent_Page_Walker(),
    ));
  }
  return '<ul class="list-page">'.$childpages.'</ul>';

}

add_shortcode('wbc_childpages', 'wbc_list_child_pages');




/**
 * Register Post type
 */


 add_action( 'init', 'cptui_register_my_taxes_destination' );
 function cptui_register_my_taxes_destination() {
 	$labels = array(
 		"name" => __( 'Destinations', '' ),
 		"singular_name" => __( 'Destination', '' ),
 		);

 	$args = array(
 		"label" => __( 'Destinations', '' ),
 		"labels" => $labels,
 		"public" => true,
 		"hierarchical" => true,
 		"label" => "Destinations",
 		"show_ui" => true,
 		"show_in_menu" => true,
 		"show_in_nav_menus" => true,
 		"query_var" => true,
 		"rewrite" => array( 'slug' => 'destination', 'with_front' => true, ),
 		"show_admin_column" => false,
 		"show_in_rest" => false,
 		"rest_base" => "",
 		"show_in_quick_edit" => false,
 	);
 	register_taxonomy( "destination", array( "tour" ), $args );

 // End cptui_register_my_taxes_destination()
 }

function wbc_register_informations() {

	/**
	 * Post Type: Informations.
	 */

	$labels = array(
		"name" => __( "Informations", "" ),
		"singular_name" => __( "Information", "" ),
	);

	$args = array(
		"label" => __( "Informations", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
    // "show_in_menu"       => "edit.php?post_type=" . WP_TRAVEL_POST_TYPE,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "information-pratiques", "with_front" => true ),
		"query_var" => true,
		"taxonomies" => array('destination'),
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "trip-informations", $args );
}

add_action( 'init', 'wbc_register_informations' );

function wbc_register_guides() {

	/**
	 * Post Type: Guides.
	 */

	$labels = array(
		"name" => __( "Guides", "" ),
		"singular_name" => __( "Guide", "" ),
	);

	$args = array(
		"label" => __( "Guides", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
    // "show_in_menu"       => "edit.php?post_type=" . WP_TRAVEL_POST_TYPE,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "trip-guides", "with_front" => true ),
		"query_var" => true,
		"taxonomies" => array('destination'),
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "trip-guides", $args );
}

add_action( 'init', 'wbc_register_guides' );

/**
 * Guide Categories Taxonomy
 */
function wbc_register_guide_cat() {

	/**
	 * Taxonomy: Guide Categories.
	 */

	$labels = array(
		"name" => __( "Guide Categories", "" ),
		"singular_name" => __( "Guide Category", "" ),
	);

	$args = array(
		"label" => __( "Guide Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Guide Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'guide-category', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "guide_cat", array( "trip-guides" ), $args );
}

add_action( 'init', 'wbc_register_guide_cat' );


//

function wbc_register_visiter() {

	/**
	 * Post Type: Visiter.
	 */

	$labels = array(
		"name" => __( "Visiter", "" ),
		"singular_name" => __( "Visiter", "" ),
	);

	$args = array(
		"label" => __( "Visiter", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
    // "show_in_menu"       => "edit.php?post_type=" . WP_TRAVEL_POST_TYPE,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "trip-visiter", "with_front" => true ),
		"query_var" => true,
		"taxonomies" => array('destination', 'post_tag'),
		"supports" => array( "title", "editor", "thumbnail", ),
	);

	register_post_type( "trip-visiter", $args );
}

add_action( 'init', 'wbc_register_visiter' );

/**
 * Guide Categories Taxonomy
 */
function wbc_register_visiter_cat() {

	/**
	 * Taxonomy: Guide Categories.
	 */

	$labels = array(
		"name" => __( "Visiter Categories", "" ),
		"singular_name" => __( "Visiter Category", "" ),
	);

	$args = array(
		"label" => __( "Visiter Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Visiter Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'visiter-category', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "visiter_cat", array( "trip-visiter" ), $args );
}

add_action( 'init', 'wbc_register_visiter_cat' );


/**
 * Enable Builder fo custom post type Guides
 */
add_action( 'init', function () {
	add_ux_builder_post_type( 'trip-guides' );
	add_ux_builder_post_type( 'trip-visiter' );
	add_ux_builder_post_type( 'trip-informations' );
} );


add_action( 'init', 'cptui_register_my_cpts_tour' );
function cptui_register_my_cpts_tour() {
	$labels = array(
		"name" => __( 'Tours', '' ),
		"singular_name" => __( 'Tour', '' ),
		);

	$args = array(
		"label" => __( 'Tours', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
				"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "tour", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title", "editor", "thumbnail" ),					);
	register_post_type( "tour", $args );

// End of cptui_register_my_cpts_tour()
}
