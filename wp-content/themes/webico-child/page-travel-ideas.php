<?php
/*
Template name: Travel - Ideas
*/
get_header(); ?>

<div id="featureds">


	<div class="banner has-hover" id="banner-featured">
          <div class="banner-inner fill">
        <div class="banner-bg fill"><div class="bg fill bg-fill  bg-loaded"></div><div class="overlay"></div></div><!-- bg-layers -->

        <div class="banner-layers container">
          <div class="fill banner-link"></div>
   					<div id="text-box-featured" class="text-box banner-layer x50 md-x50 lg-x50 y50 md-y50 lg-y50 res-text">
              <div class="text ">
								<div class="text-inner text-center">
									<h3 class="uppercase"><strong style="color:#fff;"><?php echo get_the_title(); ?></strong></h3>
									<div class="custom-divider is-divider small"></div>
              	</div>
           		</div><!-- text-box-inner -->
    			</div><!-- text-box -->
        </div><!-- .banner-layers -->
      </div><!-- .banner-inner -->
				<style scope="scope">
				#banner-featured .bg.bg-loaded {
				background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);
				}
				</style>
  </div>
	<div class="section-content intro-form">

	<div class="row row-small" id="row-1364834944">

	<div class="col medium-4 small-12 large-4"><div class="col-inner">
	<select name="destinations"><option>TYPE OF TRIP</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select>
	</div></div>
	<div class="col medium-4 small-12 large-4"><div class="col-inner">
	<select name="desire"><option>DURATION</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select>
	</div></div>
	<div class="col medium-4 small-12 large-4"><div class="col-inner">
	<button class="intro-form-button">SHOW 66 RESULTS</button>

	</div></div>


	</div>
	</div>


</div>
<div id="breadcrumbs" class="row">
<div class="col">

<?php if ( function_exists('yoast_breadcrumb') )
{yoast_breadcrumb('<div class="breadcrumb">','</div>');} ?>

</div>
</div>
<div id="parent-page" class="row">
<div class="col">
	<?php echo wbc_list_child_pages(); ?>

</div>
</div>
<div id="content" role="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>

	<?php endwhile; // end of the loop. ?>

</div>
<?php get_footer(); ?>
