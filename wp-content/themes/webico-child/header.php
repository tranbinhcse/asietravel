<!DOCTYPE html>
<!--[if IE 9 ]> <html <?php language_attributes(); ?> class="ie9 <?php flatsome_html_classes(); ?>"> <![endif]-->
<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="ie8 <?php flatsome_html_classes(); ?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="<?php flatsome_html_classes(); ?>"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); // Body classes is added from inc/helpers-frontend.php ?>>

<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'flatsome' ); ?></a>

<div id="wrapper">

<?php do_action('flatsome_before_header'); ?>
<?php if (is_front_page()) {  ?>
<div class="intro">


<?php echo do_shortcode('[block id="home-header"]');?>

<div class="section-content intro-form">

<div class="row row-collapse">

<div class="col small-12 large-12"><div class="col-inner">

<p class="intro-form-title">Move according to your desire of the moment</p>

</div></div>

</div>
<div class="row row-small" id="row-1364834944">

<div class="col medium-4 small-12 large-4"><div class="col-inner">
<select name="destinations"><option>Destinations</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select>
</div></div>
<div class="col medium-4 small-12 large-4"><div class="col-inner">
<select name="desire"><option>Your desire</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select>
</div></div>
<div class="col medium-4 small-12 large-4"><div class="col-inner">
<button class="intro-form-button">SHOW 66 RESULTS</button>

</div></div>


</div>
</div>


</div>
<?php } ?>
<header id="header" class="header <?php flatsome_header_classes();  ?>">
   <div class="header-wrapper">
	<?php
		get_template_part('template-parts/header/header', 'wrapper');
	?>
   </div><!-- header-wrapper-->
</header>

<?php do_action('flatsome_after_header'); ?>

<main id="main" class="<?php flatsome_main_classes();  ?>">
