<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'binh_asietravel');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VB8$wHLvn`_qtwF)BFYb6[cPR(GjOU=tRX={3@P6pia~j(Z``[6/ob!0]/7(%{$&');
define('SECURE_AUTH_KEY',  '&|Lux|FT([j T; z,C9rt9ui`gp?Ge[W -R5D[BXPnK)%[D))?5|,;Fv)`>L/}[M');
define('LOGGED_IN_KEY',    'j>i]Ie}t6sA*#ALD=<>5-U?[}?=Wt/o/!xvue&+Dyh?m14tx|%mO7t=ZH@!M}2te');
define('NONCE_KEY',        'G:]e[pj/!)kU,b=?t/7/~DWm%WK@nL<jhjl?|wU,+lYV--l?3Zf>u]Q.*is<G>r!');
define('AUTH_SALT',        '>2W%,D0y_JX36B9c$9p<cdcD_f/iAw8rypgO<YeDq#Y&<(#zbp.r%(Ru(l?<MT#6');
define('SECURE_AUTH_SALT', ':3cA-dnLuAmM8D5lvz4$9a}EJmUJAka@G!J&<d(bezXbT:S#[/PaHT+LV$Zx[%6-');
define('LOGGED_IN_SALT',   'v=@+-)EDlv/&4n0+i H%%1/r^40.j.|t8_Ny4AM85pac5}$@zwE1=MGH_-S!FEsc');
define('NONCE_SALT',       ';Tyi:D#%,sG&#QeBaYcnhwDXg*e|G*EmQs){qCc{Kvv-tyFl(h)b$LGEJ[HW38$k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
